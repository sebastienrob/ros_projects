#include "ros/ros.h"
#include "geometry_msgs/Pose2D.h" //get the desired position. (message)
#include "turtlesim/Pose.h"       //data of position's turtle. (message)
#include "geometry_msgs/Twist.h"  //send a velocities command. (message)
#include <math.h>

//callback function for my subscriber
void ComPoseCallback(const geometry_msgs::Pose2D::ConstPtr& msg);
void CurPoseCallback(const turtlesim::Pose::ConstPtr& msg);
float GetErrorLin(turtlesim::Pose currentpose, geometry_msgs::Pose2D desiredpose);
float GetErrorAng(turtlesim::Pose currentpose, geometry_msgs::Pose2D desiredpose);

//We receive data input but at each callback they are destroy.
//so we need a global variable for hold onto that data in main.
//so create variables of our "message data type" and copy in the callback
//global variable for my callback function
bool STOP = true;
geometry_msgs::Twist CmdVel;
turtlesim::Pose CurrentPose;
geometry_msgs::Pose2D DesiredPose;

int main(int argc, char **argv)
{
   ros::init(argc, argv, "position_control_turtle"); //connect node to roscore.
   ros::NodeHandle n; //declaration of object's node.

   //subscripter for get the desired position
   ros::Subscriber sub_desired_position = n.subscribe("/turtle1/command_position", 5, ComPoseCallback);
   //subscriber for get the current position
   ros::Subscriber sub_current_position = n.subscribe("/turtle1/pose", 5, CurPoseCallback);
   //publisher for control my turtle's position with velocities
   ros::Publisher pub_twist = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 100);

   //i want my controler of position run constantly so :
   //so we will place the code in while loop. (10Hz)
   ros::Rate loop_rate(10);
   float ErrorLin = 0;
   float ErrorAng = 0;
   ROS_INFO("Ready to send command velocities (Position Command)");

   while (ros::ok() && n.ok() )
   {
      ros::spinOnce(); //loop once time
      if (STOP == false)
      {
        ErrorLin = GetErrorLin(CurrentPose, DesiredPose);
        ErrorAng = GetErrorAng(CurrentPose, DesiredPose);
	printf("Error linear: %f, Error angular: %f\n", ErrorLin, ErrorAng);
        //pub_twist.publish(CmdVel); pour TEST1
	
	CmdVel.linear.x = 0.2 * ErrorLin; //multiple by  P for control input   
	CmdVel.angular.z = 0.5 * ErrorAng; // multiple by  P for control input 
	pub_twist.publish(CmdVel); //i send to command velocities topic.
      }
      else
      {
        printf("Waiting...\n");
      }
      loop_rate.sleep(); //sleep 10Hz -->0.1s
   }
}


// callback to send new desired Pose msgs
void ComPoseCallback(const geometry_msgs::Pose2D::ConstPtr& msg)
{
        //ROS_INFO("je reçoit mes msg de commandes");
	STOP = false;
        DesiredPose.x = msg->x;
        DesiredPose.y = msg->y;
        //CmdVel.angular.z += 1; pour TEST1
	return;
}

// callback to send new current Pose msgs
void CurPoseCallback(const turtlesim::Pose::ConstPtr& msg)
{
        //ROS_INFO("je reçoit les msg des nouvelles positions actuelles");
	CurrentPose.x = msg->x;
        CurrentPose.y = msg->y;
        CurrentPose.theta = msg->theta;
	return;
}

// function to get angular error between facing direction of the turtle and direction to desired pose
float GetErrorAng(turtlesim::Pose currentpose, geometry_msgs::Pose2D desiredpose)
{
	// create error vector
	float Ex = desiredpose.x - currentpose.x;	// Error between current x and x desired 
	float Ey = desiredpose.y - currentpose.y;	// Error between current y and y desiredY. Y component

	// get desire angle
	float dest = atan2f(Ey, Ex); 		

	// get angle error
	float E_tot = dest - currentpose.theta;

	//~ ROS_INFO("Ex: %f, Ey: %f, E_tot: %f", Ex, Ey, Et);
	return E_tot;
}

// function to get linear error from the turtle axes. Error only around turtle X axis
float GetErrorLin(turtlesim::Pose currentpose, geometry_msgs::Pose2D desiredpose)
{
	// create error vector
	float Ex = desiredpose.x - currentpose.x;	
	float Ey = desiredpose.y - currentpose.y;	
	float E_tot = GetErrorAng(currentpose, desiredpose); // get angle between vectors

	
	float E_totx = hypotf(Ex, Ey)*cos(E_tot); // c++ function : computes the square root of the sum of the squares of x and y
	
	return E_totx;
}
