# Package position_control_turtle :

> Le projet ROS pour le controle, asservissement en position de la tortue.
> Il est constitué d'une seul node comprennant :

- Un publisher pour publier la commande à recevoir pour corriger l'erreur
- Un subscriber pour récupérer la position actuelles de la tortue
- Un subscriber pour récupérer la position voulue (donner par l'utilisateur)
- deux fonctions de callbacks pour sauvegarder les messages dans des variables globales.
- deux fonctions de calculs, une pour l'erreur linéaire, l'autre pour l'erreur angulaire
- Une boucle spinonce de 10hz pour l'asservissement et correction de l'erreur avec un corecteur proportionnel P

# Commande pour le lancement manuel :
1er terminal :
Dans le repertoire catkin

- source devel/setup.zsh 
- roscore

2ème terminal :

- rosrun turtlesim turtlesim_node 

3ème terminal :

- source devel/setup.zsh 
- rosrun position_control_turtle first 

4ème terminal :

- pour allez en [x;y] = [8;8] :
rostopic pub /turtle1/command_position geometry_msgs/Pose2D "x: 8.0
y: 8.0
theta: 0.0" -1 



