# Package beginner_tutorials :

> Le tutoriel d'apprentissage pour la création d'un projet ROS comprennant : 

- Git tuto.
- Création d'un Catkin Workspace.
- Création d'un Package Catkin.
- Construction d'un ROS Package.
- Commande ROS NODE (analyser des nodes et run/test).
- ROS TOPIC + rqtGRAPH + ROSTOPIC TOOL + MESSAGE + rqtPLOT (analyser mon ROS système).
- Editer Publisher/Subscriber Node (dans src et les .cpp sont commenter).
- Création dossier msg et éditer un .msg (repertoire des messages).
- ROS SERVICE (utiliser et analyser services).
- ROS PARAM (utiliser et analyser).
- Editer une Node Serveur et une Node Client pour Service (dans src et les .cpp sont commenter).
- Construire/build ces nodes,services,messages... (run et parametrer le .xml et CMakeLists.txt)
- POUR PLUS D'INFORMATIONS SUR LES COMMANDES ET LEURS OBJECTIFS --> */src/beginner_tutorials/Commande_Infos.txt*
