#include "ros/ros.h"
#include "std_msgs/String.h"

/**
 * comment recevoir un simple message ? SUBSCRIBER NODE.
 * c'est la callback(rappel) fonction qui sera call quand un nouveau message est arrivé
 * sur un nouveau topic. Le message est transmis par boost_shared_ptr, ce qui signifie
 * que je peut la stocker si je le souhaite sans me soucier de son effacement en dessous
 * et sans copier les données sous-jacente  
 */

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node. --> ok
   */
  ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * s'abonne à un topic du nom de "chatter" avec le maitre. ROS call la fonction
   * chattercallback() chaque fois qu'un nouveau message arrive.
   * pour 1000, même principe que la node publisher, sauf qu'ici on a une file d'attente
   * maximum et si on arrive à cette limite, les anciens messages serons supprimer au fur
   * et à mesure.
   * NodeHandle::subscribe() retourne un ros::Subscriber objet que l'on doit conserver
   * jusqu'à que l'on souhaite ce désabonner.
   * quand le subscriber object est détruit, il ce désabonne automatiquement du topic.
   */

  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);


  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   * entrer dans une boucle et appeller les callback messages le plus rapidement possible.
   */

  ros::spin();


  return 0;
}


