#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
/**
 * comment envoyer un simple message ? PUBLISHER NODE.
 * ros.h pour inclure les headers necessaires pour utiliser les public pieces of ROS SYSTEME
 * std_msgs/string.h est l'header pour les string.msg files
 */

int main(int argc, char **argv)
{

  /**
   * pour le remappage des noms de node via la ligne de commande mais ici juste pour init la node
   * talker est le nom donner à la NODE.
   */
  ros::init(argc, argv, "talker");

  /**
   * crée un descripteur pour la node (point principal pour la communication).
   * le premier nodehandle initialisera la node et le dernier, détruit, nettoyera les ressources
   * utiliser par la node.
   */
  ros::NodeHandle n;


  /**
   * La fonction advertise() dit au master que nous allons publier un message de type  
   * stg_msg::String sur le topic chatter avec une taille de queue (file d'attente) de
   * 1000, c'est à dire que si on publie trop vite, le nombre maximum de messages mise dans
   * le buffer avant que les anciens soient jeté est de 1000. Cela permet de dire aux autres
   * noeuds qui écoutent ou sont subscriber, que nous allons publier de la donnée sur le topic.  
   * advertise() retourne un objet de type Publisher qui sert : d'une part à contenir une methode 
   *publish() permettant de publier des messages sur le topic et d'autre part quand il sort de
   *la porté(scope), il est automatiquement annulé.
   */
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);


  /**
   * spécifie la fréquence (loop_rate), ici à 10Hz, à laquelle je souhaite faire une boucle
   * il gardera en memoire le temps écouler depuis le dernier appel à sleep()
   * et sleep le temps impartis
   */
  ros::Rate loop_rate(10);


  /**
   * compte le nombre de messages envoyer.
   * ross::ok() retourne faux si le fameux ctrl+c, si on à était lancer sur le réseau
   * par une autre node avec le même nom, si ros::shutdown() à était appeller par une autre
   * partie de l'app ou si toutes les nodes (ros::NodeHandles) ont étaient détruite.
   */
  int count = 0;
  while (ros::ok())
  {

    /**
     * This is a message object. You stuff it with data, and then publish it.
     * on diffusse un message sur ROS en utilisant une classe adapté au message,
     * généralement généré à partir des fichiers msg. (on peut utiliser des types plus complexe.
     * ici on utilise un message standart String qui posséde un membre : data
     */
    std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();

    /**
     * ROS/INFO remplace prinf/cout
     */
    ROS_INFO("%s", msg.data.c_str());


    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     * Maintenant on transmet le message à tous ceux connecter.
     */
    chatter_pub.publish(msg);

    /**
     * pas necessaire ici (car on ne reçoit aucun callbacks(appel))
     * mais si on a une subscription et que l'ont ne la met pas alors,
     * mes callbacks(rappels) ne seront jamais appeller. (bonne mesure ici)
     */
    ros::spinOnce();

    /**
     * on utilise l'objet ros::rate pour sleep le temp restant et atteindre le 10Hz de
     * taux de publication
     */
    loop_rate.sleep();

    ++count;
  }

  return 0;
}

