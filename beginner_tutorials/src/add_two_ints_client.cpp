#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"
#include <cstdlib>
 
 int main(int argc, char **argv)
 {
   ros::init(argc, argv, "add_two_ints_client");
   if (argc != 3)
   {
     ROS_INFO("usage: add_two_ints_client X Y");
     return 1;
   }
 
   ros::NodeHandle n;
/**
*creer un client pour add_two_ints service.
*L'objet ros::ServiceClient est utilisé pour call le service ultérieurement.
*ensuite on instancie une classe service autogenerer et attribuons des valeurs à son membre de *requete.
*une classe service contient deux membres (une request et une response) et contient deux définitions de classe (une request et une reponse).
*/
   ros::ServiceClient client = n.serviceClient<beginner_tutorials::AddTwoInts>("add_two_ints");
   beginner_tutorials::AddTwoInts srv;
   srv.request.a = atoll(argv[1]);
   srv.request.b = atoll(argv[2]);
/**
*en dessous c'est l'appel au service. vu que le call de service bloquent, il reviendra une fois *le call terminer.
*Si le call abouti call() retourne vraie et la valeur de srv, la response sera validé. Au *contraire, sil le call() n'a pas abouti alors retourne faux et la valeur de srv. la réponse sera *invalide.
*/
   if (client.call(srv))
   {
     ROS_INFO("Sum: %ld", (long int)srv.response.sum);
   }
   else
   {
     ROS_ERROR("Failed to call service add_two_ints");
     return 1;
   }
 
   return 0;
 }
