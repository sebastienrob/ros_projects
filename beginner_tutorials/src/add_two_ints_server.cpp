#include "ros/ros.h"
#include "beginner_tutorials/AddTwoInts.h"

/**
*WRITTING A SERVICE NODE
*AddTwoInts.h est le header file generer par le srv file que l'on a creer avant.
*dessous la fonction fournit le service pour ajouter deux ints (elle prend en *compte le request type et response type definit dans le fichier srv et renvoie *un booleen.
*/
bool add(beginner_tutorials::AddTwoInts::Request  &req,
         beginner_tutorials::AddTwoInts::Response &res)
{
/**
*les deux ints sont ajouter et stocker dans la réponse (a et b décrit dans le *fichier dans /srv).
*certaines informations à propos de la request et response sont consignées. 
*Le service redeviens vraie une fois terminer
*/
  res.sum = req.a + req.b;
  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("sending back response: [%ld]", (long int)res.sum);
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_server");
  ros::NodeHandle n;
/**
*le service est crée et averti ROS
*/
  ros::ServiceServer service = n.advertiseService("add_two_ints", add);
  ROS_INFO("Ready to add two ints.");
  ros::spin();

  return 0;
}

