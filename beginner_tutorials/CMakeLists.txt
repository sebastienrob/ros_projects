cmake_minimum_required(VERSION 2.8.3)
project(beginner_tutorials)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  genmsg
)

## Declare ROS messages and services
add_message_files(DIRECTORY msg FILES Num.msg)
add_service_files(DIRECTORY srv FILES AddTwoInts.srv)

## Generate added messages and services
##génerent toutes les targets des messages, services.
generate_messages(DEPENDENCIES std_msgs) ##ou d'autres paquets contenant des msgs

##declare un catkin package.
##pour etre sur que la commande catkin_package() déclarent bien.
##les dépendances des messages, services pour les autres packages. 
catkin_package(CATKIN_DEPENDS message_runtime std_msgs)

## Build talker and listener.
include_directories(include ${catkin_INCLUDE_DIRS})

##création de l'executable dans le devel.
add_executable(talker src/talker.cpp)
## s'assurent que les header files des messages soient générer avant utilisation.
##utilisant des messages d'un autre package, on a besoin d'ajouter les dependances
##à leurs generation targets.
target_link_libraries(talker ${catkin_LIBRARIES})
##ajoutent les dépendances des nodes (executables target) avec les services/##messages (message generation target).
add_dependencies(talker beginner_tutorials_generate_messages_cpp)

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
add_dependencies(listener beginner_tutorials_generate_messages_cpp)

## Build service client and server
add_executable(add_two_ints_server src/add_two_ints_server.cpp)
target_link_libraries(add_two_ints_server ${catkin_LIBRARIES})
add_dependencies(add_two_ints_server beginner_tutorials_gencpp)

add_executable(add_two_ints_client src/add_two_ints_client.cpp)
target_link_libraries(add_two_ints_client ${catkin_LIBRARIES})
add_dependencies(add_two_ints_client beginner_tutorials_gencpp)



